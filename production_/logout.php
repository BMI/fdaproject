<?php
session_start();
$_SESSION = array();

//Delete all session scopes
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

//Deletes Temp Directory 
$tmpdir = '/tmp/FDATemp/';
$sessid_ext = session_id();
$tmpdir_sessid_ext = $tmpdir . $sessid_ext . '/';

function recursiveRemove($dir) {
    $structure = glob(rtrim($dir, "/").'/*');
    if (is_array($structure)) {
        foreach($structure as $file) {
            if (is_dir($file)) recursiveRemove($file);
            elseif (is_file($file)) unlink($file);
        }
    }
    rmdir($dir);
}
if(is_dir($tmpdir_sessid_ext))
{
recursiveRemove($tmpdir_sessid_ext);
}
session_destroy();
header("Location: index.php"); 
?>

