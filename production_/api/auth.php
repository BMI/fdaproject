<?php session_start();

require_once 'HTTP/Request2.php';
require_once("config.php");
$user = $_REQUEST['username'];
$user = urlencode($user);
$password = $_REQUEST['password'];
$password = urlencode($password);

$nbiaResponse = getResponse($nbiaRealm, $user, $password);
$aimeResponse = getResponse($aimeRealm, $user, $password);

try {
	          if (200 == $nbiaResponse->getStatus() && 200 == $aimeResponse->getStatus()) 
	          {
			     $nbia_json_output = json_decode($nbiaResponse->getBody());
				 $aime_json_output = json_decode($aimeResponse->getBody());
				 $_SESSION['username'] = $user;
				 $_SESSION['nbia_api_key'] = $nbia_json_output->api_key;
				 $_SESSION['aime_api_key'] = $aime_json_output->api_key;
		         $_SESSION['nbia_service'] = $fdaNBIAService;
		         $_SESSION['aime_service'] = $fdaAIMEService;
				 $_SESSION['logged_in'] = true;
		         
				        if(isset($_GET['target'])){
							$target = $_GET['target'];
							$target = str_replace('/', '', $target);
							header("Location: ../" . $target);
						}
						else if(!isset($_GET['target'])){
							header("Location: ../index.php?request=valid");	
						}
		         
		      } 
		      else 
		      {	
		      	if(isset($_GET['target']))
		      	{
		      	$target = $_GET['target'];
			    $target = str_replace('/', '', $target);
			    $target = urlencode($target);
		        header("Location: ../index.php?error=Invalid&target=" . $target );
		        }
		        else{
			    header("Location: ../index.php?error=Invalid" );  	  
		        }
		      }
} 
catch (HTTP_Request2_Exception $e) {
echo 'Error: ' . $e->getMessage();
}

function getResponse($realmUrl, $username, $pass)
{
    $request = new HTTP_Request2("http://$username:$pass@$realmUrl");

    return $response = $request->send();
}
?>
