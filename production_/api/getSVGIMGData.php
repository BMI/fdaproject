   <?php

    function getIMGSVGData($theURL, $dir_path, $extract_path, $_format){
            
          //Grab Zip from REST Call
          $ch = curl_init($theURL);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $data = curl_exec($ch);
          curl_close($ch);

          //Iniitiate time for dir
          $milliseconds_path = round(microtime(true) * 1000);
          
          //Make payload path dir
          $path = $dir_path . $milliseconds_path . '/';
          mkdir($path);
          
          $path_zip = $path . 'getPNGSVG.zip';
          
          file_put_contents($path_zip, $data);
          $extract_path = $path  . 'getPNGSVG/';
          
          //Extract zip from zip to extracted path
          $zip = new ZipArchive;
          $zip->open($path_zip);
          $zip->extractTo($extract_path);
          $zip->close();
          //Delete zip
          unlink($path_zip);

          $imgdir = $extract_path . '*.{' . $_format . '}';
          $dicomimg = glob($imgdir , GLOB_BRACE);
          $js_img_path =  str_replace('/tmp', '', $dicomimg);

          $svgdir = $extract_path . '*.{xml}';
          $svgxml = glob($svgdir, GLOB_BRACE);
          $js_svgpath = str_replace('/tmp', '', $svgxml);
          
          return array($js_img_path, $js_svgpath);
    }

    ?>