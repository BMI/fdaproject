<?php
session_start();
?>


<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);
include_once("RestRequest.php");

## Get your data url here
require_once("config.php"); 

//From config.php
$getImageUIDurl  = $RESTURLgetAllPNGSVGByPatientID;
$api_key = $_SESSION['aime_api_key'];
$dataurl = $getImageUIDurl . $api_key;

$pid = $_GET['pid'];
$dataurl = $dataurl . '&patientId=' . $pid;

$getDataRequest = new RestRequest($dataurl, 'GET');
$getDataRequest->execute();
$JSONdata = json_decode($getDataRequest->responseBody);

$window = '';
$level = '';
$format = 'jpeg';
$scale = '';
$width = 512;


//array of IMAGE_UID
$arrayofdates = array();
$arrayofJSON = array();
//Return Arrays
$imageArray = array();
$svgArray = array();
$payloadArray = array();

 $tmpdir = '/tmp/FDATemp/';
        $sessid_ext = session_id();
        $tmpdir_sessid_ext = $tmpdir . $sessid_ext . '/' ;
        $pid_ext = $pid;
        $tmpdir_sessid_ext_pid_ext = $tmpdir_sessid_ext . $pid_ext . '/';
        if (!is_dir($tmpdir_sessid_ext_pid_ext)) {
        mkdir($tmpdir_sessid_ext_pid_ext);  
        }    

// create an array to hold all image_uids
$image_uids = array();   
// created an array to hold all study dates
$study_dates = array();

foreach ($JSONdata as $value){
    array_push($study_dates, $value->STUDYDATE);
    $array_of_images;
    if(!isset($image_uids[$value->STUDYDATE]))
    {
        $array_of_images = array();
    }
    else
    {
     $array_of_images = $image_uids[$value->STUDYDATE];
    }

    array_push($array_of_images , $value->IMAGE_UID);
    $image_uids[$value->STUDYDATE] = $array_of_images;
}
sort($study_dates);

foreach ($study_dates as $date) {
    $uids = $image_uids[$date];
   
   foreach ($uids as $uid) {

      // create a temp directory for uid
        $area_ext = 'A_';
        $uid_ext = $uid;
        $tmpdir_sessid_ext_pid_ext_uid_ext = $tmpdir_sessid_ext_pid_ext . $area_ext . $uid_ext . '/';
//Make payload path dir
if (!file_exists ($tmpdir_sessid_ext_pid_ext_uid_ext)) {
mkdir($tmpdir_sessid_ext_pid_ext_uid_ext);
}
// create a path to store the zip file 
$path_zip = $tmpdir_sessid_ext_pid_ext_uid_ext . 'getPNGSVG.zip';
// make the rest call to grap contents of zip
$getImagesbyUIDURL  = $RESTURLgetPNGSVG . $_SESSION['nbia_api_key'] . '&uid=' . $uid . '&window=400&level=40&format=' . $format . '&width=' . $width ;
//Grab Zip from REST Call
        $ch = curl_init($getImagesbyUIDURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
// store the zip file
file_put_contents($path_zip, $data); 
// extract the contents of zip file
$extract_path = $tmpdir_sessid_ext_pid_ext_uid_ext  . 'getPNGSVG/';
                $zip = new ZipArchive;
                $zip->open($path_zip);
                $zip->extractTo($extract_path);
                $zip->close();
                unlink($path_zip);


 $imgdir = $extract_path . '*.{' . $format . '}';
 $dicomimg = glob($imgdir , GLOB_BRACE);
 $js_img_path =  str_replace('/tmp', '', $dicomimg);

 $svgdir = $extract_path . '*.{xml}'; 
 $svgxml = glob($svgdir, GLOB_BRACE);
 $js_svgpath = str_replace('/tmp', '', $svgxml);

 $uidObject = array();
 $uidObject["imgpath"] = $js_img_path;
 $uidObject["svgpath"] = $js_svgpath;

 $payloadArray[$uid] = $uidObject;
   }
}
echo(json_encode($payloadArray));
