<?php

$NBIA = 'http://fdanbia.cci.emory.edu:9099';
$NBIA_MONGO = 'http://fdanbia.cci.emory.edu:9099';

$AIME = 'http://fdaaime.cci.emory.edu:9099';
$AIME_SR = 'http://192.168.2.26:9099';

//Image Database
//NBIA
$fdaNBIAService = $NBIA . '/services/fda/nbia/';
//MongoDB
$fdaMongoService = $NBIA_MONGO . '/services/fda/imaging/';

//AIM Database
//AIME XML
$fdaAIMEService = $AIME . '/services/fda/aime/';
//AIME DICOM-SR
$fdaAIMESRService = $AIME_SR . '/services/FDA/AIME-SR/';

$nbiaRealm = 'fdanbia.cci.emory.edu:9099/securityTokenService';
$aimeRealm = 'fdaaime.cci.emory.edu:9099/securityTokenService';

//index.php
//fdanbia
$RESTURLpatientDataforTable = $_SESSION['nbia_service'] . 'query/patientList?api_key=';

//index.php
//fdaaime
$RESTURLgetAllPNGSVGByPatientID = $_SESSION['aime_service'] . 'query/getImageIDByPatient?api_key=';

//show_dicom_img_annotation.php
//fdanbia
$RESTURLgetPNGSVG = $_SESSION['nbia_service'] . 'query/getPngSvg?api_key=';

//show_dicom_report.php
//fdaaime
$RESTURLgetPatientReportData = $_SESSION['aime_service'] . 'query/getAnnotationByPatientID?api_key=';

?>
