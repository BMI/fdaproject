<?php session_start();
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);
include_once("RestRequest.php");

## Get your data url here
require_once("config.php"); 

//From config.php
$url  = $RESTURLpatientDataforTable;
//fdanbia
$api_key = $_SESSION['nbia_api_key'];
$dataurl = $url . $api_key;

$getDataRequest = new RestRequest($dataurl, 'GET');
$getDataRequest->execute();
$data = json_decode($getDataRequest->responseBody);

echo(json_encode($data));
?>
