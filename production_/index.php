<?php
session_start();
if(isset($_GET['error'])){
$error = '* Invalid Login. Please Try Again.';	
}
else{
$error = '';
}
?>

<?php

if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] = true){
$title = 'FDA AIM-DICOM Federation Pilot';

//Create directory in tmp
$tmpdir = '/tmp/FDATemp/';
if (!is_dir($tmpdir)) {
    mkdir($tmpdir);  
}

  $sessid_ext = session_id();
  //Create sub-directory in tmp with session id 
  $tmpdir_sessid_ext = $tmpdir . $sessid_ext . '/';
      if (!is_dir($tmpdir_sessid_ext)) {
          mkdir($tmpdir_sessid_ext);  
      }
}

$config = '';
if(isset($_GET['configuration']) && $_GET['configuration'] == 'updated'){
$config = true;
}

?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
<script>

var config = <?php echo json_encode($config); ?>;
$.getJSON("api/dataRetreiverJSON.php", function(displayData){
        jsonResult = displayData; 
        showTable(jsonResult, config);
});
</script>
<?php include('includes/header.php'); ?>

<!-- Specific Page Styles -->
<!-- <link href="css/index.less" rel="stylesheet/less"> -->
<link href="css/index.css" rel="stylesheet">

<!-- Start Page Content -->
    <div class="loader">
        <img src="img/ajax-loader.gif" />
    </div><!-- loader -->
    
<div class="container" >
<?php if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == true){ ?>
	
	<div class="modal_no_annotations">
      <div class="inside_no_annotations">
        <a href="#" class="close_modal_no_annotations"><img src="img/close__.png" /></a>
        <p>&ldquo; Sorry Patient <span class="no_message" style="color:#333;"></span> Has No Annotations Available at This Time. &rdquo;</p>
      </div>
    </div><!-- no_annotations -->
    
    <div class="modal_win">
      <div class="inside_modal_win">
          <div class="modal_heading">
            <h4 style="width: 20%; margin-left: 10px;"><span class="modal_pat_name"></span></h4>
            <div class="modal_measure_" style="width: 55%;">
              <div class="measure_">
              </div><!-- measure_ -->
            </div>
            <a style="width: 25%; margin-right: 10px;" href="#" class="close_modal"><img src="img/close__.png" /></a>
          </div><!-- modal_heading -->

          <div class="left_arrow">
            <img src="img/lft_arrow.png" />
          </div><!-- left_arrow -->
          
          <div class="measure_placeholder">
            <div id="measure_">
            </div><!-- svg_ -->
          </div><!-- measure_placeholder -->

          <div class="img_placeholder" id="">
            <div id="image_">
              
            </div><!-- image_ -->
          </div><!-- img_placeholder -->

          <div class="svg_placeholder">
            <div id="svg_">
            </div><!-- svg_ -->
          </div><!-- svg_placeholder -->

          <div class="right_arrow">
            <img src="img/rht_arrow.png" />
          </div><!-- right_arrow -->

      </div><!-- inside_modal_win -->
    </div><!-- modal_win -->
        
    <div id="output_table">
	<div class="data_table_report_heading">
     <h2>
        <span class="drug_output"></span>
        <span class="company_output"></span>
        <span class="config"></span>
    </h2>
    </div><!-- report_heading -->
    </div><!-- output_table -->
    
<?php } else { ?>
    
    <div class="fda_info">
      <div class="inside_fda_info">
        <h4>FDA Pilot Information</h4>
        <p>This effort is intended to be a basic proof-of-concept to demonstrate technical feasibility of storing and accessing images and their associated annotations to facilitate FDA review. We propose using an anonymized set of images and annotations associated with mock clinical trial data that will be tabulated and described on PDF files, resembling study reports a sponsor might submit to FDA to support documentation of efficacy of a new drug. The files will be “submitted” to FDA evaluators.  These files will have mock patient data that would ordinarily be expected to be linked to image and annotation data.</p>
        <a href="https://wiki.nci.nih.gov/display/Imaging/FDA+Demo+Pilot+Project" target="_blank" class="btn btn-primary">Learn More</a>
      </div><!-- inside_fda_info -->
    </div><!-- fda_info -->

    <div class="login-form">
          <h2>Login</h2>
         <?php if(isset($_GET['target'])) { ?>
         <?php $target = $_GET['target']; ?>
         <form action="api/auth.php?target=<?php echo urlencode($target) ?>" method="POST">
         <?php } else { ?>
         <form action="api/auth.php" method="POST">
         <?php } ?>
           <fieldset>
              <div class="clearfix">
                <input type="text" placeholder="Username" name="username" class="login_name">
              </div>
              <div class="clearfix">
                <input type="password" placeholder="Password" name="password" class="login_password">
              </div>
              <button class="btn btn-primary" type="submit">Sign in</button>
            </fieldset>
          </form>
          <div class="error_">
	          <p style="color:#d14; margin-top:-7px; font-size:13px;"><?php echo $error; ?></p>
          </div><!-- error_ -->
    </div><!-- login-form -->
<?php } ?>



</div> <!-- /container -->

    <!-- Dependencies -->
    <script src="js/bootstrap.js"></script>
    <script src="js/modernizr-2.6.2.min.js"></script>

    <!-- dev js  -->
    <script src="js/index.js"></script>
    <script src="js/ie_detection.js"></script>

   </body>
</html>
