<?php
session_start();

if (!isset($_SESSION['logged_in'])) {
$target = "$_SERVER[REQUEST_URI]"; 
header("Location: index.php?target=" . urlencode($target)); 
}


?>

<?php
require_once("api/config.php"); 
$pid = $_GET['pid'];
$title = 'FDA Report for Patient: ' . $pid;
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
<script>
    var pid = <?php echo json_encode($pid); ?>;
    $.get("api/dataRetreiverXML.php?pid=" + pid, function(displayData){

          xmlResult = displayData; 
          outputReport(xmlResult);
          
          breadCrumb_pid = <?php echo json_encode($pid); ?>;
          getBreadCrumb(breadCrumb_pid);
    });
</script>


<?php include('includes/header.php'); ?>

<!-- Specific Page Styles -->
<link href="css/report.css" rel="stylesheet">

<!-- Start Page Content -->
    <div class="loader">
        <img src="img/ajax-loader.gif" />
    </div><!-- loader -->

    <div class="container" >

     <div id="breadcrumb_container"style="margin-top:60px;" >  
      <ul class="breadcrumb">
        <li>
        <a href="index.php">Listing</a>
        <span class="divider">/</span>
       </li>
        <li>
        <span class="currentpidnameBC"></a>
        </li>
      </ul>
    </div>
    
    <div class="report_heading">
        <h2><span class="report_clin_head"></span> <span class="drug_output"></span></h2>
    </div><!-- report_heading -->
    <div id="report_container">
        <div id="report_output">
            <div class="adobe_icon">
            </div><!-- adobe_icon -->
            <div class="inside_report_output_heading">
            </div><!-- inside_report_output -->
            <div class="inside_report_output_table">
            </div><!-- inside_report_output -->
        </div><!-- report_output -->
    </div><!-- report_container -->
    </div> <!-- /container -->

    <!-- Dependencies -->
    <script src="js/bootstrap.js"></script>
    <script src="js/modernizr-2.6.2.min.js"></script>

    <!-- dev js  -->
    <script src="js/breadcrumb.js"></script>
    <script src="js/report.js"></script>
    <script src="js/ie_detection.js"></script>

  </body>
</html>
