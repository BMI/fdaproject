
 function outputReport(xmlResult){
      
      //Temp data
      var rep_clin_head = "Clinical Study Report for";
      var rep_header_drug_ = "Drug ABC123";
      var rep_company = "Acme Pharmaceuticals";

      $('.report_clin_head').text(rep_clin_head);
      $('.drug_output').text(rep_header_drug_);

      var rep_out_pdf = '<a title="Print Report" href="javascript:newWindowPDF(pid, 1000, 475)" class="callpdf">';
          rep_out_pdf += '<img src="img/print__.png"/>';
          rep_out_pdf += '</a>';
      $('.adobe_icon').append(rep_out_pdf);

      var rep_out_top = '<h4>' + rep_company + '</h4>';
          rep_out_top += '<p><span class="bold">Patient:</span>&nbsp;' + pid + '</p>';
          rep_out_top += '<div class="measure_key"><p style="font-size:12px;"><span class="bold">&mu;</span>&nbsp;= Mean</p>';
          rep_out_top += '<p style="font-size:12px;><span class="bold">&sigma;</span>&nbsp;= Standard Deviation</p></div>';
      $('.inside_report_output_heading').append(rep_out_top);
     
      var rep_out_table = '<table>';
          rep_out_table += '<tr class="heading_">';
          rep_out_table += '<td class="heading_"><h2>Study Date</h2></td>';
          rep_out_table += '<td class="heading_"><h2>Reviewer</h2></td>';
          rep_out_table += '<td class="heading_"><h2>Equipment</h2></td>';
          rep_out_table += '<td class="heading_"><h2>Length</h2></td>';
          rep_out_table += '<td class="heading_"><h2>Area</h2></td>';
          rep_out_table += '<td class="heading_"><h2><span style="font-size:18px;">&mu;</h2></td>';
          rep_out_table += '<td class="heading_"><h2><span style="font-size:18px;">&sigma;</span></h2></td>';
          rep_out_table += '<td class="heading_"><h2>Image(s)</h2></td>';
          rep_out_table += '</tr>';

          //Parse XML from PHP string
           xmlResult = $.parseXML(xmlResult);

           var row_found = $(xmlResult).find('Row');
           row_found = row_found.length;

           if(row_found === 0){
               $('.adobe_icon').hide();
               $('.measure_key').hide();

               var rep_out_table = '';
               rep_out_table += '<p>** Patient ' + pid + ' Has No Annotations Available at This Time.</p>';
               rep_out_table += ' <a href="index.php" class="btn btn-primary">&laquo; Back to Listing</a>';

               $('.no_message').text(pid);
               
               $('.inside_report_output_table').append(rep_out_table);
           }
           else{
            //Send XML to array
            var results = [];
            $(xmlResult).find('Row').each(function() {
                var dateText = $(this).find('ImageStudy').attr('startDate');
                var dateVal = Date.parse(dateText.replace(/\./g, '/'));
                
                dateText = new Date(Date.parse(dateText));
                dateText = dateText.toString().replace(/UTC\s/,"");
                dateText = dateText.replace(/GMT.+/,"");
                dateText = dateText.toString();
                dateText = dateText.split(" ");

                var calculation = $(this).find('Calculation');
                var num_of_calculation = calculation.length;
                var value = $(this).find('CalculationData').attr('value');
                value = parseFloat(value);
                value = value.toFixed(2);

                var length_;
                var area;
                var mean;
                var stan_dev;
                var calcArray=[];

                
                if(num_of_calculation == 1){
                  length_ = value += ' mm';
                  area = 'N/A';
                  mean = 'N/A';
                  stan_dev = 'N/A';
                }

                else if(num_of_calculation == 3){
                  
                  $.each(calculation, function(){
                    var calcValue = $(this).find("CalculationData").attr("value");
                    calcArray.push(calcValue);
                  });

                    length_ = 'N/A';
                    area = calcArray[0];
                    mean = calcArray[1];
                    stan_dev = calcArray[2];
                    
                    area = parseFloat(area);
                    area = area.toFixed(2);
                    mean = parseFloat(mean);
                    mean = mean.toFixed(2);
                    stan_dev = parseFloat(stan_dev);
                    stan_dev = stan_dev.toFixed(2);

                    area += ' mm';
                    mean += ' HU';
                    stan_dev += ' HU';
                }
              

                results.push({
                    date: dateText,
                    dateVal: dateVal,
                    reviewer: $(this).find('User').attr('name'),
                    equipment: $(this).find('Equipment').attr('manufacturerModelName'),
                    sopInstUID: $(this).find('Image').attr('sopInstanceUID'),
                    length_: length_,
                    area: area,
                    mean: mean,
                    stan_dev: stan_dev
                });
            });
            //Sort XML by Date
            results.sort(function(a, b) {
                return a.dateVal > b.dateVal;
            });

            $.each(results, function() {
              rep_out_table += '<tr class="sop_link">'; 
               rep_out_table += '<td>' + this.date[1] + ' ' + this.date[2] + ' ' +this.date[3] + '</td>';
               rep_out_table += '<td>' + this.reviewer + '</td>';
               rep_out_table += '<td>' + this.equipment + '</td>';
               rep_out_table += '<td>' + this.length_ +'</td>';
               rep_out_table += '<td>' + this.area +'</td>';
               rep_out_table += '<td>' + this.mean +'</td>';
               rep_out_table += '<td>'+ this.stan_dev +'</td>';
               rep_out_table += '<td><a class="image_go" href="show_img_annotation.php?pid=' + pid + '&uid=' + this.sopInstUID + '">Image(s)</a></td>';
              rep_out_table += '</tr>';
            });

          rep_out_table += '</table>';
          rep_out_table += '<a href="index.php" class="btn btn-primary">&laquo; Back to Listing</a>';
          $('.inside_report_output_table').append(rep_out_table);
          

          $('.sop_link').mouseover(function(){
              $(this).css('background-color', '#E0EEEE');
          });
           $('.sop_link').mouseout(function(){
              $(this).css('background-color', '');
          });

        $('.image_go').click(function(){
          $('.white_overlay').fadeIn(100);
          $('.loader').fadeIn(100);
        });
    }
}

function newWindowPDF(pid, w, h){
    wLeft = window.screenLeft ? window.screenLeft : window.screenX;
    wTop = window.screenTop ? window.screenTop : window.screenY;
    var left = wLeft + (window.innerWidth / 2) - (w / 2);
    var top = wTop + (window.innerHeight / 2) - (h / 2);
    return window.open("show_report_print.php?pid=" + pid, "FDA Report", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}


    function popup_outputReport(xmlResult){
      
      //Temp data
      var rep_clin_head = "Clinical Study Report for";
      var rep_header_drug_ = "Drug ABC123";
      var rep_company = "Acme Pharmaceuticals";

      $('.report_clin_head').text(rep_clin_head);
      $('.drug_output').text(rep_header_drug_);

      var rep_out_top = '<h4>' + rep_company + '</h4>';
          rep_out_top += '<p><span class="bold">Patient:</span>&nbsp;' + pid + '</p>';
          rep_out_top += '<div class="measure_key"><p style="font-size:12px;"><span class="bold">&mu;</span>&nbsp;= Mean</p>';
          rep_out_top += '<p style="font-size:12px;><span class="bold">&sigma;</span>&nbsp;= Standard Deviation</p></div>';
      $('.inside_report_output_heading').append(rep_out_top);
     
      var rep_out_table = '<table>';
          rep_out_table += '<tr class="heading_">';
          rep_out_table += '<td class="heading_"><h2>Study Date</h2></td>';
          rep_out_table += '<td class="heading_"><h2>Reviewer</h2></td>';
          rep_out_table += '<td class="heading_"><h2>Equipment</h2></td>';
          rep_out_table += '<td class="heading_"><h2>Length</h2></td>';
          rep_out_table += '<td class="heading_"><h2>Area</h2></td>';
          rep_out_table += '<td class="heading_"><h2><span style="font-size:18px;">&mu;</h2></td>';
          rep_out_table += '<td class="heading_"><h2><span style="font-size:18px;">&sigma;</span></h2></td>';
          rep_out_table += '<td class="heading_"><h2>Image(s)</h2></td>';
          rep_out_table += '</tr>';

          //Parse XML from PHP string
           xmlResult = $.parseXML(xmlResult);
          
          //Send XML to array
            var results = [];
            $(xmlResult).find('Row').each(function() {
                var dateText = $(this).find('ImageStudy').attr('startDate');
                var dateVal = Date.parse(dateText.replace(/\./g, '/'));
                
                dateText = new Date(Date.parse(dateText));
                dateText = dateText.toString().replace(/UTC\s/,"");
                dateText = dateText.replace(/GMT.+/,"");
                dateText = dateText.toString();
                dateText = dateText.split(" ");

                var calculation = $(this).find('Calculation');
                var num_of_calculation = calculation.length;
                var value = $(this).find('CalculationData').attr('value');
                value = parseFloat(value);
                value = value.toFixed(2);
                var length_;
                var area;
                var mean;
                var stan_dev;
                var calcArray=[];

                
                if(num_of_calculation == 1){
                  length_ = value += ' mm';
                  area = 'N/A';
                  mean = 'N/A';
                  stan_dev = 'N/A';
                }

                else if(num_of_calculation == 3){
                  
                  $.each(calculation, function(){
                    var calcValue = $(this).find("CalculationData").attr("value");
                    calcArray.push(calcValue);
                  });

                    length_ = 'N/A';
                    area = calcArray[0];
                    mean = calcArray[1];
                    stan_dev = calcArray[2];
                    
                    area = parseFloat(area);
                    area = area.toFixed(2);
                    mean = parseFloat(mean);
                    mean = mean.toFixed(2);
                    stan_dev = parseFloat(stan_dev);
                    stan_dev = stan_dev.toFixed(2);

                    area += ' mm';
                    mean += ' HU';
                    stan_dev += ' HU';
                }
              

                results.push({
                    date: dateText,
                    dateVal: dateVal,
                    reviewer: $(this).find('User').attr('name'),
                    equipment: $(this).find('Equipment').attr('manufacturerModelName'),
                    sopInstUID: $(this).find('Image').attr('sopInstanceUID'),
                    length_: length_,
                    area: area,
                    mean: mean,
                    stan_dev: stan_dev
                });
            });
            //Sort XML by Date
            results.sort(function(a, b) {
                return a.dateVal > b.dateVal;
            });
            console.log(results);

            $.each(results, function() {
              rep_out_table += '<tr class="sop_link">'; 
               rep_out_table += '<td>' + this.date[1] + ' ' + this.date[2] + ' ' +this.date[3] + '</td>';
               rep_out_table += '<td>' + this.reviewer + '</td>';
               rep_out_table += '<td>' + this.equipment + '</td>';
               rep_out_table += '<td>' + this.length_ +'</td>';
               rep_out_table += '<td>' + this.area +'</td>';
               rep_out_table += '<td>' + this.mean +'</td>';
               rep_out_table += '<td>'+ this.stan_dev +'</td>';
               rep_out_table += '<td><a class="image_go" href="show_img_annotation.php?pid=' + pid + '&uid=' + this.sopInstUID + '" style="text-decoration:underline;">Image(s)</a></td>';
              rep_out_table += '</tr>';
            });

          rep_out_table += '</table>';
          $('.inside_report_output_table').append(rep_out_table);

          rep_out_button = '<a href="javascript:window.close()"" class="btn btn-primary" style="margin-left:655px; margin-right:5px;">Close</a>';
          rep_out_button += '<a href="javascript:window.print()" class="btn btn-primary print">Print</a>';
           $('.buttons_bottom').append(rep_out_button);
  }




 


