

    function showTable(jsonResult, config){

        var listData = jsonResult;
        //temp data
        data_header_drug_ = "Drug ABC123";
        data_header_drug_ = data_header_drug_ + ' /';
        data_company = "Acme Pharmaceuticals";
        $('.drug_output').text(data_header_drug_);
        $('.company_output').text(data_company);
        
         if(typeof config !== 'undefined' && config === true)
            {
              var config_updated = 'Configuration Updated!';
              $('.config').text(config_updated);
              $('.config').css('color' , '#d14');
              $('.config').css('font-weight' , 'normal');
              $('.config').css('font-size' , '14px');
              $('.config').css('margin-left' , '45px');
              $('.config').fadeOut(2000);    
            }
        

        var output = '<table>';
        output += '<tr class="heading_">';
        output += '<td class="heading_"><h2>Patient</h2></td>';
        output += '<td class="heading_"><h2>Sex</h2></td>';
        output += '<td class="heading_"><h2>Images</h2></td>';
        output += '<td class="heading_"><h2>Report</h2></td>';
        output += '</tr>';

         for (var i=0; i<listData.length; i++){
          output += '<tr class="tr_out">';
          output += '<td>' + listData[i].PatientID + '</td>';
            if(listData[i].PatientSex == 0000){
          output += '<td>N/A</td>';
              }
            else
              {
          output += '<td>' + listData[i].PatientSex + '</td>';
              }
          output += '<td><a class="init_modal" href="#" id="' + listData[i].PatientID + '">Image(s)</a></td>';
          output += '<td><a class="show_loading" id="' + listData[i].PatientID + '" href="show_report.php?pid='
          + listData[i].PatientID + '">Report</a></td>';
          output += '</tr>';
        }
        output += '</table>';
        $('#output_table').append(output);

         $('.tr_out').mouseover(function(){
              $(this).css('background-color', '#E0EEEE');
          });
         $('.tr_out').mouseout(function(){
              $(this).css('background-color', '');
          });

        $('.show_loading').click(function(){
          $('.white_overlay').fadeIn(100);
          $('.loader').fadeIn(100);
        });

        //Modal Call
        $('.init_modal').click(function(){
        //Modal init
         $('.white_overlay').fadeIn(300);
         $('.loader').fadeIn(100);
            var pid = $(this).attr('id');
          $.getJSON("api/getmodalData.php?pid=" + pid, function(displaymodalData){
              callModalImagesSVG(displaymodalData, pid);
            });
        });

  }//showTable

  function callModalImagesSVG(displaymodalData, pid){

        if (displaymodalData.length === 0) {

          $('.loader').hide(100);
          $('.modal_no_annotations').fadeIn(300);
          $('.no_message').text(pid);

          //Close Modal
          $('.close_modal_no_annotations').click(function(){
            var no_annotations = true;
            closeModalViewer(no_annotations);
          });

        }

        else{

        $('.modal_win').fadeIn(300);
        $('.modal_pat_name').text(pid);
        var images = [];
        var the_svgs = [];

        $.each(displaymodalData, function(key, value) {
        
            img = value.imgpath;
            images.push(img);
            svg = value.svgpath;

              $.ajax({
                    async: false,
                    type: 'GET',
                    url: svg,
                    success: function(data) {
                      the_svgs.push(data);
                    }
                });
        });
        
          var amount_of_images = images.length;
          var index = 0;
    
          init_image = '<img src="' + images[0] + '" id="the_Image"/>';
          $('#image_').append(init_image);
          outputSVGAnnotation(the_svgs[0]);
          showMeasurements(the_svgs[0]);
          hideShowArrows(index, amount_of_images);
          Image_ = $('#the_Image');

          //Control Events
          $(".right_arrow").click(function () {
            Image_.fadeOut('fast', function() {
                  $(this).fadeIn('fast', function()
                  {
                  index++;
                  $(this).attr('src', images[index]);
                  $('.svg_').remove(); 
                  $('.measure_').remove(); 
                  outputSVGAnnotation(the_svgs[index]);
                  showMeasurements(the_svgs[index]);
                  hideShowArrows(index, amount_of_images);
                  });
              });
          });

               $(".left_arrow").click(function () {
                Image_.fadeOut('fast', function() {
                      $(this).fadeIn('fast', function()
                      {
                        if(index == 0){
                          index;
                        }
                        else
                        {
                        index--;
                        }
                        $(this).attr('src', images[index]);
                        $('.svg_').remove(); 
                        $('.measure_').remove(); 
                        outputSVGAnnotation(the_svgs[index]);
                        showMeasurements(the_svgs[index]);
                        hideShowArrows(index, amount_of_images);
                      });
                  }); 
              });
        
        //Close Modal
        $('.close_modal').click(function(){
            $('#the_Image').remove();
            $('.svg_').remove();  
            $('.measure_').remove(); 
            images = [];
            the_svgs = [];
            closeModalViewer();
        });
      }
    }//callModalImagesSVG

     function hideShowArrows(index, amount_of_images){
        
        var cut_off = amount_of_images-1;
        
        if(index === 0){
         $('.left_arrow').css('visibility', 'hidden');
         $('.right_arrow').css('visibility', 'visible');
        }
        if(index > 0){
         $('.left_arrow').css('visibility', 'visible');
         $('.right_arrow').css('visibility', 'visible');
        }
        if(index === cut_off){
         $('.right_arrow').css('visibility', 'hidden');
        }
     }

     function outputSVGAnnotation(the_svgs){
       
            var svg_out = '';

            $(the_svgs).find('line').each(function(){  
              poly = $(this);
              var seri = new XMLSerializer();
              svg_out += '<div class="svg_">';
              svg_out += '<svg xmlns="http://www.w3.org/2000/svg" style="height:100%; width:100%;">';
              svg_out += seri.serializeToString(poly[0]); 
              svg_out += '</svg>'; 
            });
    
            $(the_svgs).find('polygon').each(function(){  
              poly = $(this);
              var seri = new XMLSerializer();
              svg_out += '<div class="svg_">';
              svg_out += '<svg xmlns="http://www.w3.org/2000/svg" style="height:100%; width:100%;">';
              svg_out += seri.serializeToString(poly[0]); 
              svg_out += '</svg>'; 

            });

            $(the_svgs).find('polyline').each(function(){  
              poly = $(this);
              var seri = new XMLSerializer();
              svg_out += '<div class="svg_">';
              svg_out += '<svg xmlns="http://www.w3.org/2000/svg" style="height:100%; width:100%;">';
              svg_out += seri.serializeToString(poly[0]); 
              svg_out += '</svg>'; 

            });

            $(the_svgs).find('ellipse').each(function(){  
              poly = $(this);
              var seri = new XMLSerializer();
              svg_out += '<div class="svg_">';
              svg_out += '<svg xmlns="http://www.w3.org/2000/svg" style="height:100%; width:100%;">';
              svg_out += seri.serializeToString(poly[0]); 
              svg_out += '</svg>'; 

            });

            $('#svg_').append(svg_out);

            dressSVG();

     }//outputSVGAnnotation

     function showMeasurements(the_svgs){
              var measure_out = ''; 
              
              $(the_svgs).find('desc').each(function(){  
                  measure = $(this);
                  var seri = new XMLSerializer();
                  value = seri.serializeToString(measure[0]);
                  value = value.replace('<desc xmlns="http://www.w3.org/2000/svg">', "");
                  value = value.replace("</desc>", "");
                  type = value.split(" ");

                      if(type[0] == 'Standard'){
                        type[0] = 'Standard Deviation:';
                        the_type = type[0];
                        the_num = type[2];
                        the_unit = type[3];
                      }
                      else{
                        the_type = type[0];
                        the_num = type[1];
                        the_unit = type[2];
                      }

                  the_num = parseFloat(the_num);
                  the_num = the_num.toFixed(2);

                  measure_out += '<div class="measure_">';
                  measure_out += '<p>';
                  measure_out += the_type;
                  measure_out += '&nbsp;';
                  measure_out += the_num; 
                  measure_out += '&nbsp;';
                  measure_out += the_unit;  
                  measure_out += '</p>';
                  measure_out += '</div>';
              });
              $('#measure_').append(measure_out);
      }

      function dressSVG(){

            $('line').attr('stroke-width', '3');
            $('polyline').attr('stroke-width', '3');
            $('polygon').attr('stroke-width', '3');
            $('ellipse').attr('stroke-width', '3');

      }
      
      function closeModalViewer(no_annotations){
          if(typeof no_annotations !== 'undefined' && no_annotations === true)
          {
            $('.modal_no_annotations').fadeOut(300);
            $('.white_overlay').fadeOut(400);
          }
          else
          {
            $('.loader').fadeOut(100); 
            $('.modal_win').fadeOut(300);
            $('.white_overlay').fadeOut(400);
          }
     }//closeModalViewer