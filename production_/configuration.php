<?php
session_start();

require_once("api/config.php");

if (!isset($_SESSION['logged_in'])) {
$target = "$_SERVER[REQUEST_URI]"; 
header("Location: index.php?target=" . urlencode($target)); 
}

$selectedNBIA = $_SESSION['nbia_service'];
$selectedAIME = $_SESSION['aime_service'];


?>

<?php
$title = 'FDA AIM-DICOM Federation Pilot Configuration';
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>

<?php include('includes/header.php'); ?>

<!-- Specific Page Styles -->
<link href="css/configuration.css" rel="stylesheet">

<!-- Start Page Content -->
  
<div class="container" style="margin-top:60px;">

     <!-- Breadcrumb -->
    <div id="breadcrumb_container">  
      <ul class="breadcrumb">
        <li>
        <a href="index.php">Listing</a>
        <span class="divider">/</span>
        </li>
        <li>
        Configuration
        </li>
      </ul>
    </div>

    
    <div class="config_heading">
        <h2>Configuration</h2>
    </div><!-- report_heading -->
    <div id="config_container">
        <div id="config_output">
          <div id="config_form">
          <h2>Services</h2>
            <form action="api/processConfig.php" method="POST">
            <div class="inside_form">
              <h4>Image Database</h4>
                <div class="radio">
                  <label>
                    <input type="radio" name="image_database" id="image_db_nbia" value="image_db_nbia"
                    <?php if($selectedNBIA == $fdaNBIAService){ ?>
                    checked
                    <?php } ?>
                    >
                    NBIA
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="image_database" id="image_db_mongo" value="image_db_mongo"
                    <?php if($selectedNBIA == $fdaMongoService){ ?>
                    checked
                    <?php } ?>
                    >
                    MongoDB
                  </label>
                </div>
                <div class="spacer"></div>
                <h4>AIM Database</h4>
                <div class="radio">
                  <label>
                    <input type="radio" name="aim_database" id="aim_db_xml" value="aim_db_xml"
                    <?php if($selectedAIME == $fdaAIMEService){ ?>
                    checked
                    <?php } ?>
                    >
                    AIME XML
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="aim_database" id="aim_database_dicom_sr" value="aim_database_dicom_sr"
                    <?php if($selectedAIME == $fdaAIMESRService){ ?>
                    checked
                    <?php } ?>
                    >
                    AIME DICOM-SR
                  </label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="index.php" class="btn btn-danger">Cancel</a>
             </div><!-- inside_form -->
          </form>
        </div>

           <div id="text_config_output">
            <p>This pilot project is setup to retrieve images from either NBIA or a NoSQL database. Each of these image stores have a RESTful web service with an API. The annotations are accessible from a service that consumes and produces AIM documents in an XML format, or from a different service that consumes and produces AIM documents as DICOM Structured Reports. From this page, you have the option to switch between NBIA or the NoSQL database (MongoDB) as the image store. Similarly, you can switch between an AIME XML or an AIME DICOM-SR as the default store for annotations.<br /><br />
	           The image services store images from the NSCLC Radiogenomics collection that was uploaded to TCIA. This collection contains images from patients with non-small cell lung cancer imaged prior to surgical excision with both thin-section CT and whole body PET/CT scans acquired under IRB approval from Stanford University and the Veterans Administration Palo Alto Health Care System.<br /><br />
	          <span style="font-size:15px;">The Annotation services store annotations that were made by Dr. Ross Filice for this project.</span> 
	        </p>
        </div><!-- text_config_output -->

        </div><!-- config_output -->
    </div><!-- config_container -->

</div> <!-- /container -->

    <!-- Dependencies -->
    <script src="js/bootstrap.js"></script>
    <script src="js/modernizr-2.6.2.min.js"></script>
    
    <script src="js/ie_detection.js"></script>

  </body>
</html>
