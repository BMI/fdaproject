<?php
session_start();

if (!isset($_SESSION['logged_in'])) {
$target = "$_SERVER[REQUEST_URI]"; 
header("Location: index.php?target=" . urlencode($target)); 
}

?>

<?php

require_once("api/config.php"); 

//From config.php
$url  = $RESTURLgetPNGSVG;
//$api_key = $RESTURLgetPNGSVGAPIKEY;
$api_key = $_SESSION['nbia_api_key'];

$pid = $_GET['pid'];

//For PNGSVG
$authen_url = $url . $api_key;

$uid = $_GET['uid'];
$window = '';
$level = '';
$format = 'jpeg';
$scale = '';
$width = 512;

$title = 'FDA Report for Patient: ' . $pid .' & Image and Annotations for SOPInstanceUID: ' . $uid;

//Temp directory for files from index.php
$tmpdir = '/tmp/FDATemp/';
$sessid_ext = session_id();
$tmpdir_sessid_ext = $tmpdir . $sessid_ext . '/';

$tmpdir_sessid_ext = $tmpdir . $sessid_ext . '/';
$pid_ext = $pid;
$tmpdir_sessid_ext_pid_ext = $tmpdir_sessid_ext . $pid_ext . '/';
  if (!is_dir($tmpdir_sessid_ext_pid_ext)) {
    mkdir($tmpdir_sessid_ext_pid_ext);  
    } 

$area_ext = 'C_';
$uid_ext = $uid;
$tmpdir_sessid_ext_uid_ext =  $tmpdir_sessid_ext_pid_ext . $area_ext . $uid_ext . '/';
if(!is_dir($tmpdir_sessid_ext_uid_ext)){
    mkdir($tmpdir_sessid_ext_uid_ext);
}
$extractpath_one = '';

//Set URL fo REST IMG SVG
$authen_url = $authen_url . '&uid=' . $uid;

//Image
$img_svg_load_one = $authen_url . '&window=350&level=40&format=' . $format . '&width=' . $width;
//Lung
$img_svg_load_two = $authen_url . '&window=1500&level=-600&format=' . $format . '&width=' . $width;
//Bone
$img_svg_load_three = $authen_url . '&window=2500&level=480&format=' . $format . '&width=' . $width;

//Call getIMGSVGData
require_once("api/getSVGIMGData.php");

$output_one = getIMGSVGData($img_svg_load_one, $tmpdir_sessid_ext_uid_ext, $extractpath_one, $format);

$output_two = getIMGSVGData($img_svg_load_two, $tmpdir_sessid_ext_uid_ext, $extractpath_one, $format);

$output_three = getIMGSVGData($img_svg_load_three, $tmpdir_sessid_ext_uid_ext, $extractpath_one, $format);

//[0]-Image [1]-SVG Annotation
$total_image_output = array($output_one[0], $output_two[0], $output_three[0]);

?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>

  <script>
    var pid = <?php echo json_encode($pid); ?>;
    var uid = <?php echo json_encode($uid); ?>;

     $.get("api/dataRetreiverXML.php?pid=" + pid, function(displayData){

          xmlResult = displayData; 
          showAnnotationText(xmlResult);

          var theSVGDataPath = <?php echo json_encode($output_one[1]); ?>;
          var ImgFiles = <?php echo json_encode($total_image_output); ?>;
          var numImgFiles = ImgFiles.length;

          showImages(ImgFiles, numImgFiles); 
          outputSVGAnnotation(theSVGDataPath, numImgFiles);
          showMeasurements(theSVGDataPath);

          breadCrumb_uid = <?php echo json_encode($uid); ?>;
          breadCrumb_pid = <?php echo json_encode($pid); ?>;
          getBreadCrumb(breadCrumb_pid, breadCrumb_uid);
    });

  </script>

<?php include('includes/header.php'); ?>

<!-- Specific Page Styles -->
<link href="css/img_annotation.css" rel="stylesheet">
  
<!-- Start Page Content -->

    <div class="container" style="margin-top:60px;">

       <!-- Breadcrumb -->
    <div id="breadcrumb_container">  
      <ul class="breadcrumb">
        <li>
        <a href="index.php">Listing</a>
        <span class="divider">/</span>
        </li>
        <li>
        <a class="report_bc_link" href="">Report for <span class="currentpidnameBC"></a>
        <span class="divider">/</span>
        </li>
        <li>
        <span class="currentuidBC"></a>
        </li>
      </ul>
    </div>
    
    <div id="annotation_container">
    <!-- Text Annotation output -->
      <div id="annotation_out">
      </div><!-- annotation_out -->
      <div class="spacer"></div>
      <div id="measure_annotation_out">
      </div><!-- measure_annotation_out -->
       <div id="button_">
    </div><!-- button_ -->
    </div><!-- annotation_container -->
    
     <div id="image_container">
  
      <!-- Dicom Image output -->
         <div id="output_image">
         </div><!-- output_image -->
       <!-- SVG annotation output -->
         <div id="output_svg">
         </div><!-- output_svg -->
    </div><!-- image_container -->
      <div class="back_to_top">
        <img src="img/up_arrow.png" />
       </div><!-- back_to_top -->
    </div> <!-- /container -->


    <div class="footer">
    </div>

    <!-- Dependencies -->
    <script src="js/bootstrap.js"></script>
    <script src="js/modernizr-2.6.2.min.js"></script>
    
    <!-- dev js  -->
    <script src="js/img_annotation.js"></script>
    <script src="js/breadcrumb.js"></script>
    <script src="js/ie_detection.js"></script>

  </body>
</html>
