<?php session_start();
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);
include_once("RestRequest.php");

## Get your data url here
require_once("config.php"); 

//From config.php
$url  = $RESTURLgetPatientReportData;
$api_key = $_SESSION['aime_api_key'];

$pid = $_GET['pid'];
$url = $url . $api_key . '&patientID=' . $pid;	
$dataurl = $url;
$data = file_get_contents($dataurl);
echo $data;

?>
