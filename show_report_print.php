<?php
session_start();

if (!isset($_SESSION['logged_in'])) {
header("Location: index.php"); 
}

?>

<?php
require_once("api/config.php"); 
$pid = $_GET['pid'];
$title = 'FDA Report for Patient: ' . $pid; 
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

   <!--  Cross App Styles -->
    <link href="css/normalize.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    
    
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
<script>
    var pid = <?php echo json_encode($pid); ?>;
    $.get("api/dataRetreiverXML.php?pid=" + pid, function(displayData){

          xmlResult = displayData; 
          popup_outputReport(xmlResult);

    });
</script>
<!-- Specific Page Styles -->
<link href="css/report.css" rel="stylesheet">
<link href="css/print_main.css" rel="stylesheet" media="print" />
<link href="css/print_report.css" rel="stylesheet" media="print" />
<style>
@media print {
  a[href]:after {
    content: none !important;
  }
}
.inside_report_output_table td
    {
    width: 150px;
    text-align: center;
    padding-top: 8px;
    padding-bottom: 8px;
    font-size: 12px;
    border-right: 1px solid #999;
    }
</style>
<!-- Start Page Content -->

    <div class="container" >

    <div class="report_heading">
        <h2><span class="report_clin_head"></span> <span class="drug_output"></span></h2>
    </div><!-- report_heading -->
    <div id="report_container">
        <div id="report_output">
            <div class="adobe_icon">
            </div><!-- adobe_icon -->
            <div class="inside_report_output_heading">
            </div><!-- inside_report_output -->
            <div class="inside_report_output_table">
            </div><!-- inside_report_output -->
        </div><!-- report_output -->
    </div><!-- report_container -->
    <div class="buttons_bottom">
    </div><!-- buttons_bottom -->
    </div> <!-- /container -->

    <!-- Dependencies -->
    <script src="js/bootstrap.js"></script>
    <script src="js/modernizr-2.6.2.min.js"></script>

    <!-- dev js  -->
    <script src="js/report.js"></script>
    <script src="js/ie_detection.js"></script>

  </body>
</html>
