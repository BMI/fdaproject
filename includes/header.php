
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

   <!--  Cross App Styles -->
    <link href="css/normalize.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    
    
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>
  
    <div class="white_overlay">
    </div><!-- white_overlay -->

   <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="#">FDA AIM-DICOM Federation Pilot</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
            </ul>
             <ul class="nav" style="float:right;">
              <li>
              <a href="https://wiki.nci.nih.gov/display/Imaging/FDA+Demo+Pilot+Project" target="_blank">Help</a>
              </li>
              <?php if ($_SESSION['logged_in'] == true){ ?>
              <li>
              <a href="configuration.php">Config</a>
              </li>
               <li>
               <a href="logout.php">Logout</a>
               </li>
               <?php } ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

