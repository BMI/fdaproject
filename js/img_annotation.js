 function showImages(ImgFiles, numImgFiles){

        var level_types =["Chest","Lung","Bone"];
        var ww = [350, 1500, 2500];
        var wL = [40, 600, 480];
        
        var img_out = '';
        for(i=0; numImgFiles>i; i++){
                img_out += '<div class="image_">';
                img_out += '<img src="' + ImgFiles[i] + '"  id=""/>';
                img_out += '<div class="levels">';
                img_out += '<table>';
                img_out += '<tr><td>Type:</td><td style="font-weight:normal; color:#0088cc; font-size:13px;">' + level_types[i] + '</td></tr>';
                img_out += '<tr><td>ww:</td><td style="font-weight:normal; color:#0088cc; font-size:13px;">' + ww[i] + '</td></tr>';
                img_out += '<tr><td>wL:</td><td style="font-weight:normal; color:#0088cc; font-size:13px;">' + wL[i] + '</td></tr>';
                img_out += '</table>';
                img_out += '</div>';
                img_out += '</div>';
              }
        $('#output_image').append(img_out);

        $('.image_').css('opacity', '0.0');
        $('.image_').animate({
              opacity: 1,
            }, 500, function() {
        });

      scrollToTop();

     }//showImages

    function outputSVGAnnotation(theSVGDataPath, numImgFiles){

        $.get(theSVGDataPath, function(thePData){  
            data = thePData;
            var svg_out = '';  
            
            $(data).find('line').each(function(){  
              poly = $(this);
              var seri = new XMLSerializer();
              svg_out += '<div class="svg_">';
              svg_out += '<svg xmlns="http://www.w3.org/2000/svg" style="height:100%; width:100%;">';
              svg_out += seri.serializeToString(poly[0]); 
              svg_out += '</svg>';
          });
    
            $(data).find('polygon').each(function(){  
              poly = $(this);
              var seri = new XMLSerializer();
              svg_out += '<div class="svg_">';
              svg_out += '<svg xmlns="http://www.w3.org/2000/svg" style="height:100%; width:100%;">';
              svg_out += seri.serializeToString(poly[0]); 
              svg_out += '</svg>'; 

            });

            $(data).find('polyline').each(function(){  
              poly = $(this);
              var seri = new XMLSerializer();
              svg_out += '<div class="svg_">';
              svg_out += '<svg xmlns="http://www.w3.org/2000/svg" style="height:100%; width:100%;">';
              svg_out += seri.serializeToString(poly[0]); 
              svg_out += '</svg>'; 

            });

             $(data).find('ellipse').each(function(){  
              poly = $(this);
              var seri = new XMLSerializer();
              svg_out += '<div class="svg_">';
              svg_out += '<svg xmlns="http://www.w3.org/2000/svg" style="height:100%; width:100%;">';
              svg_out += seri.serializeToString(poly[0]); 
              svg_out += '</svg>'; 

            });

            
            for(var i=0; numImgFiles>i; i++){
              $('#output_svg').append(svg_out);
             }

             dressSVG();

        });
    }//outputSVGAnnotation


   function showAnnotationText(xmlResult){
    
    patient_name_ = pid;
    var ann_out = '<p><span class="bold">Patient:</span>&nbsp;' + patient_name_ + '</p><br />';
    //Parse XML from PHP string
    xmlResult = $.parseXML(xmlResult);

    $(xmlResult).find("ImageAnnotation").each(function(){
        var sopInstUID = $(this).find('Image').attr('sopInstanceUID');
          if(sopInstUID == uid){
                    var dateTime = $(this).find('ImageStudy').attr('startDate');
                    
                    dateTime = new Date(Date.parse(dateTime));
                    dateTime = dateTime.toString().replace(/UTC\s/,"");
                    dateTime = dateTime.replace(/GMT.+/,"");
                    dateTime = dateTime.toString();
                    dateTime = dateTime.split(" ");

                    var reviewer = $(this).find('User').attr('name');
                    var equipment = $(this).find('Equipment').attr('manufacturerModelName');
                        
     ann_out += '<p><span class="bold">Study Date:</span>&nbsp;' + dateTime[1] + ' ' + dateTime[2] + ' ' + dateTime[3] + '</p><br />';
      ann_out += '<p><span class="bold">Reviewer:</span>&nbsp;' + reviewer + '</p><br />';
      ann_out += '<p><span class="bold">Equipment:</span>&nbsp;' + equipment + '</p><br />';

              }
      });
      $('#annotation_out').append(ann_out);

  }//showAnnotationText

  function showMeasurements(theSVGDataPath){
      $.get(theSVGDataPath, function(thePData){  
              data = thePData;  
              var measure_out = ''; 

              $(data).find('desc').each(function(){  
                  measure = $(this);
                  var seri = new XMLSerializer();
                  value = seri.serializeToString(measure[0]); 
                  value = value.replace('<desc xmlns="http://www.w3.org/2000/svg">', "");
                  value = value.replace("</desc>", "");
                  type = value.split(" ");

                  //console.log(type);

                      if(type[0] == 'Standard'){
                        type[0] = '&sigma;:';
                        the_type = type[0];
                        the_num = type[2];
                        the_unit = type[3];
                      }

                      else if(type[0] == 'Mean:'){
                        type[0] = '&mu;:';
                        the_type = type[0];
                        the_num = type[1];
                        the_unit = type[2];
                      }

                      else{
                        the_type = type[0];
                        the_num = type[1];
                        the_unit = type[2];
                      }

                  the_num = parseFloat(the_num);
                  the_num = the_num.toFixed(2);

                  measure_out += '<p>';
                  if(the_type == '&mu;:' || the_type == '&sigma;:'){
                  measure_out += '<span style="font-weight:bold; font-size: 15px;">' + the_type + '</span>'; 
                  }
                  else{
                  measure_out += '<span style="font-weight:bold;">' +the_type + '</span>';
                  }
                  measure_out += '&nbsp;';
                  measure_out += the_num; 
                  measure_out += '&nbsp;';
                  measure_out += the_unit;  
                  measure_out += '</p><br />';

              });
            $('#measure_annotation_out').append(measure_out);
             
             var button_out = '';
             button_out += '<a style="margin-top:10px; margin-left:-10px; margin-bottom:10px;" href="show_report.php?pid=' + pid +'" class="btn btn-primary">&laquo; Back to Report for ' + pid +'</a>';
            $('#button_').append(button_out);
      });
  }

  function dressSVG(){

            $('line').attr('stroke-width', '3');
            $('polyline').attr('stroke-width', '3');
            $('polygon').attr('stroke-width', '3');
            $('ellipse').attr('stroke-width', '3');

      }
  
  function scrollToTop(){

    $(window).bind("scroll", function() {
        position_ = $(this).scrollTop();

        if (position_ > 50) {
          $('.back_to_top').fadeIn(100);
        }
        else if(position_ < 50) {
          $('.back_to_top').fadeOut(100);
        }
    });

     $('.back_to_top').click(function(){
        $("html, body").animate({
                        scrollTop: 0}
                        , 600);
    });
  }



