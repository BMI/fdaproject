FDA App Notes

~ temp dir for storage of payloads from REST -> http://domain/FDATemp/

~ On application login directory for payloads made /tmp/FDATemp/sessionID

~ Scripts for REST Call data -> /api/

~ Configuration for REST call URL -> /api/config.php

~ Setting of API keys for appending to REST URL, Checking for user login 	credentials -> /api/auth.php

~ Current deployment in -> /production_/

	/fdaproject/production_/index.php -> Login form directs to /api/auth.php for user authentication and getting API keys for storing in session

	/fdaproject/index.php -> Login form credentials: Username: test
										 			 Password: test

~ Deployment/Local Dev
	Edit httpd.conf to make directory for alias /tmp/FDATemp -> FDATemp
	sudo chown -R _www FDATemp
	Set ownership of temp directory to apache -> _www




